terraform {
  backend "remote" {
    organization = "platinum"
    workspaces {
      name = "platinum"
    }
  }
  required_providers {
    tfe = {
      version = "~> 0.26.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.7.0"
    }
  }
}

provider "tfe" {
  token = var.tfe_api_token
}

provider "gitlab" {
  token = var.gitlab_access_token
}
