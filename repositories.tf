data "gitlab_group" "infra_platinum" {
  full_path = "infra-platinum"
}

locals {
  platinum_name = "platinum-infra"
}

resource "gitlab_project" "platinum" {
  name         = "terraform-${local.platinum_name}"
  description  = "Workspace ${local.platinum_name}"
  namespace_id = data.gitlab_group.infra_platinum.group_id

  visibility_level           = "public"
  tags                       = ["terraform_managed", "terraform"]
  default_branch             = "main"
  request_access_enabled     = false
  wiki_enabled               = false
  snippets_enabled           = false
  container_registry_enabled = false
  lfs_enabled                = false
  initialize_with_readme     = true
}
